# Git/Github Workshop at CVL - Fall 2019

This contains the presentation, a markdown file of all the codes in the presentation, and a link to the ebook (bookdown) of the workshop.

This workshop is co-run by Micaela [@mychan24](https://github.com/mychan24) & Ekarin [@epongpipat](https://github.com/epongpipat).

Access the eBook version of the workshop here: https://gitbookdown.site

Some materials for this workshop was borrowed [Software Carpentry's course](https://swcarpentry.github.io/git-novice/). Check them out and many other tutorials they offer.
